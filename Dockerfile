FROM node:lts
ARG NPM_PROXY=https://registry.npmjs.org/

LABEL project=kanod-dashboard
WORKDIR /app

COPY package*.json app.ts tsconfig.json ./
COPY dashboard ./dashboard
COPY public ./public
COPY views ./views
COPY scss  ./scss
RUN npm config set registry $NPM_PROXY && npm config set proxy $http_proxy && npm config set https-proxy $https_proxy && npm install && npm run build

CMD [ "node", "app.js" ]
EXPOSE 3000
