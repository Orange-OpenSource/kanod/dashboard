# Dashboard

A very simple dashboard that lists clusters and for each cluster lists the
node.

Implemented as a nodejs application running in a pod. The current version is
synchronous and will not scale.
