/*
Copyright 2021 Orange.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
import * as path from "path";
import * as bodyParser from "body-parser";
import express, {} from "express";
import * as handlebars from "express-handlebars";
import passport from "passport";
import morgan from "morgan";
import session from "express-session";
import * as winston from "winston";
import { get_bmhs, get_bmh } from "./dashboard/bmhs";
import { get_baremetalpools, get_baremetalpool, watch_baremetalpools, use_baremetalpools } from "./dashboard/baremetalpool";
import { get_clusters } from "./dashboard/clusterdefs";
import { get_cluster, get_kubeconfig } from "./dashboard/clusters";
import { get_apps } from "./dashboard/applications";
import { protect, protect_admin } from "./dashboard/session";



const port = 3000;
const app = express();

const rootDir = __dirname;

// LOGGING
export const logger = winston.createLogger({
    transports: [
        new winston.transports.Console(),
    ]
});


// HANDLEBAR SETUP
const hbsInstance = handlebars.create({
    defaultLayout: "main.hbs",
    extname: "hbs",
    layoutsDir: path.join(rootDir, "views", "layouts"),
    helpers: {
        ifeq: function(v1: string, v2: string, options: any) {
            if(v1 === v2) {
              return options.fn(this);
            }
            return options.inverse(this);
        },
        megabytes: (v: string) => {
            let x = parseInt(v, 10);
            if (x < 1000) return `${x} MB`
            x /= 1000;
            if (x < 1000) return `${x} GB`
            x = Math.floor(x) / 1000;
            return `${x} TB`
        },
        bytes: (v: string) => {
            let x = parseInt(v, 10);
            if (x < 1000) return `${x} B`
            x /= 1000;
            if (x < 1000) return `${x} kB`
            x = Math.floor(x) / 1000;
            if (x < 1000) return `${x} MB`
            x = Math.floor(x) / 1000;
            if (x < 1000) return `${x} GB`
            x = Math.floor(x) / 1000;
            return `${x} TB`
        }
    },
});


app.set("views", path.join(rootDir, "views"));
app.engine("hbs", hbsInstance.engine as any);
app.set("view engine", "hbs");
// static part
app.use(express.static(path.join(rootDir, "public")));

/* For logging request to the server */
app.use(morgan("combined"));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false,
}));
const flash = require('connect-flash');
app.use(flash());

app.use(session({
    secret: "kanod-secret"
}));
app.use(passport.initialize());
app.use(passport.session());



function get_root(req: express.Request<any>, res: express.Response<any>) {
    res.render(
        'root.hbs',
        {
            user: req.user?.username,
            isAdmin: req.user?.username == 'admin',
            scripts: [],
            bmpSupport: use_baremetalpools
        }
    )
}

function get_login(req: express.Request<any>, res: express.Response<any>) {
    res.render('login.hbs', {
        user: req.user?.username,
        message: req.flash('error') });
}

function get_logout(req: express.Request<any>, res: express.Response<any>) {
    req.logout({}, console.log);
    res.redirect('/login');
}

// will silently return if bmp are not supported.
watch_baremetalpools();

app.get('/', protect, get_root);
app.get('/logout', get_logout);
app.get('/login', get_login);
app.get('/clusters', protect, get_clusters);
app.get('/apps', protect, get_apps);
app.get('/clusters/:cluster', protect, get_cluster);
app.get('/clusters/:cluster/kubeconfig', protect, get_kubeconfig);
app.get('/bmhs', protect_admin, get_bmhs);
app.get('/baremetalpools', protect, get_baremetalpools)
app.get('/baremetalpools/:namespace/:pool', protect, get_baremetalpool)
app.get('/bmhs/:namespace/:bmh', protect, get_bmh);
app.post('/login',
    passport.authenticate('local', {
        successRedirect: '/',
        failureRedirect: '/login',
        failureFlash: true
    })
);
app.listen(port, () => logger.info(`Listening on ${port}`));
