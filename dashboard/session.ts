/*
Copyright 2021 Orange.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

import * as fs from "fs";
import * as readline from "readline";
import * as bcrypt from "bcrypt";
import express from "express";
import passport from "passport";
import { Strategy } from "passport-local";
import * as winston from "winston";

import * as k8s from "@kubernetes/client-node";
import {coreApi} from "./core";

declare global {
    namespace Express {
        interface User {
            username: string;
        }
    }
}

let saved_hash = '';

function base64(input: string): string {
    const buffer = Buffer.from(input, 'ascii');
    return buffer.toString('base64');
}

async function inject_argocd_password(hash: string) {
    const patch = [
        {
            "op": "replace",
            "path":"/data/admin.password",
            "value": base64(hash)
        }
    ];
    const options = {
        "headers": { "Content-type": k8s.PatchUtils.PATCH_FORMAT_JSON_PATCH}
    };
    winston.log('info', 'injecting admin password in argocd');
    await coreApi.patchNamespacedSecret(
        'argocd-secret', 'argocd', patch,
        undefined, undefined, undefined, undefined,
        options)
    saved_hash = hash;
}

async function findPasswordHash(user: string): Promise<string | undefined> {
    try {
        const fileStream = fs.createReadStream(process.env.AUTH_FILE || '/mnt/passwords');
        const rl = readline.createInterface({
            input: fileStream,
            crlfDelay: Infinity
        });
        for await (const line of (rl as any)) {
            const fields = line.split(':');
            if (fields.length == 2 && fields[0] == user) {
                const hash = fields[1].replace(/[\r\n]/g, '');
                if (user == 'admin' && hash != saved_hash) {
                    await inject_argocd_password(hash);
                }
                return hash;;
            }
        }
    } catch (e) {
        winston.log(
            'info',
            `Cannot check the password. May be passwords not mounted: ${e}`);
    }
    return;
}

passport.use(new Strategy(
    { usernameField: 'username' },
    async (username, password, done) => {
        const msg = 'Incorrect username or password';
        const hash = await findPasswordHash(username); // users[username];
        if (hash === undefined) {
            done(null, false, { message: msg });
        } else {
            bcrypt.compare(password, hash).then(
                result => {
                    if (result) { done(null, { username: username }); }
                    else {
                        done(null, false, { message: msg });
                    }
                }).catch(err => done(null, false, { message: msg }))
        }
    }
));

passport.serializeUser(function (user, done) {
    done(null, user.username);
});

passport.deserializeUser(function (user, done) {
    done(null, { username: user as string });
});

export function protect(
    req: express.Request<any>, res: express.Response<any>,
    next: () => void) {
    if (req.user) {
        return next();
    }
    res.redirect("/login");
}

export function protect_admin(
    req: express.Request<any>, res: express.Response<any>,
    next: () => void) {
    if (req.user?.username == 'admin') {
        return next();
    }
    res.redirect("/login");
}
