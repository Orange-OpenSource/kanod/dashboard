/*
Copyright 2021 Orange.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

import * as k8s from "@kubernetes/client-node";

// BASE K8S CLIENT APIS
export const kc = new k8s.KubeConfig();
if (process.env.KC_DEBUG) {
    kc.loadFromFile(process.env.KC_DEBUG);
} else {
    kc.loadFromCluster();
}

export const coreApi = kc.makeApiClient(k8s.CoreV1Api);
export const customApi = kc.makeApiClient(k8s.CustomObjectsApi);
export const client = k8s.KubernetesObjectApi.makeApiClient(kc);
