/*
Copyright 2021 Orange.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

import * as express from "express";
import * as k8s from "@kubernetes/client-node";
import {customApi, coreApi} from "./core";
import {get_clusterdef, Namespace} from "./clusterdefs";
import { RejectionHandler } from "winston";
import { use_baremetalpools } from "./baremetalpool";


const CAPI_VERSION = "v1beta1"

/** Base64 decoding of a string */
export function unbase64(input: string): string {
    const buffer = Buffer.from(input, 'base64');
    return buffer.toString('ascii');
}

interface DeploymentStatus {
    requested: number,
    launched: number,
    ready: number
}

interface ClusterLightSpec<T> {
    kcp?: T,
    deployments: { [name: string]: T },
}

async function get_replicas(cname: string): Promise<ClusterLightSpec<DeploymentStatus>> {
    const result = <ClusterLightSpec<DeploymentStatus>>{ kcp: undefined, deployments: {} };
    try {
        const kcp_list = await customApi.listClusterCustomObject(
            "controlplane.cluster.x-k8s.io", CAPI_VERSION, "kubeadmcontrolplanes",
            undefined, undefined, undefined,
            `cluster.x-k8s.io/cluster-name=${cname}`);

        const kcps = (<any>kcp_list.body).items;
        if (kcps.length == 1) {
            const kcp = kcps[0];
            const kcp_status = {
                requested: <number>kcp.spec.replicas,
                launched: <number>kcp.status.replicas,
                ready: <number>kcp.status.readyReplicas,
            };
            result.kcp = kcp_status;
        }
    } catch (e) {
        console.log(`Error while retrieving control-plane: ${e}`);
    }
    try {
        const md_list = await customApi.listClusterCustomObject(
            "cluster.x-k8s.io", CAPI_VERSION, "machinedeployments",
            undefined, undefined, undefined,
            `cluster.x-k8s.io/cluster-name=${cname}`);
        const mds = (<any>md_list.body).items;
        for (const md of mds) {
            const md_status = {
                requested: <number>md.spec.replicas,
                launched: <number>md.status.replicas,
                ready: <number>md.status.readyReplicas,
            };
            result.deployments[md.metadata.name] = md_status;
        }
    } catch (e) {
        console.log(`Error while retrieving deployments: ${e}`);
    }
    return result;

}

interface NodeLight {
    name?: string,
    kubelet?: string,
    runtime?: string,
    image?: string,
    kernel?: string,
    ready: boolean,
    pressure: string[]
}

async function target_kubeconfig(cluster_name: string, namespace: string): Promise<string | undefined> {
    const kubeconfig_secret = await coreApi.readNamespacedSecret(
        `${cluster_name}-kubeconfig`, namespace,
    );
    const field = kubeconfig_secret.body.data!.value;
    if (field) return unbase64(field);
}

async function clusterNodes(
    cluster_name: string,
    namespace: string
): Promise<ClusterLightSpec<NodeLight[]>> {
    const result: ClusterLightSpec<NodeLight[]> = {
        kcp: [],
        deployments: {}
    }
    try {
        const kubeconfig = await target_kubeconfig(cluster_name, namespace);
        if (!kubeconfig) return result;
        console.log('At kubeconfig');
        const kcTarget = new k8s.KubeConfig();
        kcTarget.loadFromString(kubeconfig);
        const targetApi = kcTarget.makeApiClient(k8s.CoreV1Api);
        const nodes = await targetApi.listNode();
        for (const node of nodes.body.items) {
            const lightNode: NodeLight = {
                name: node.metadata!.name,
                kubelet: node.status!.nodeInfo!.kubeletVersion,
                runtime: node.status!.nodeInfo!.containerRuntimeVersion,
                image: node.status!.nodeInfo!.osImage,
                kernel: node.status!.nodeInfo!.kernelVersion,
                ready: false,
                pressure: []
            }
            if (node.status && node.status!.conditions) {
                for (const condition of node.status.conditions) {
                    if (condition.type == "Ready") {
                        lightNode.ready = (condition.status == "True");
                    } else if (condition.status == "True") {
                        lightNode.pressure.push(condition.type[0]);
                    }
                }
            }
            if (node?.metadata?.labels) {
                if (node.metadata.labels["node-role.kubernetes.io/master"] != undefined) {
                    result.kcp?.push(lightNode);
                } else {
                    const md_name = node.metadata.labels["cluster.kanod.io/part-of"];
                    if (md_name) {
                        (result.deployments[md_name] || (result.deployments[md_name] = [])).push(lightNode);
                    }
                }
            }
        }
    } catch (e) {
        console.log(`Error fetching nodes: ${e}`);
    }
    return result;
}

export async function get_kubeconfig(req: express.Request<any>, res: express.Response<any>) {
    try {
        const cdef_name = req.params.cluster;
        const cdef = get_clusterdef(cdef_name, req.user?.username);
        const namespace = cdef?.spec?.multiTenant ? `${Namespace}-${cdef_name}` : Namespace
        const kubeconfig = await target_kubeconfig(cdef_name, namespace);
        if (kubeconfig) {
            res.setHeader('Content-Type', 'text/plain');
            res.setHeader('Cache-Control', 'no-store');
            res.send(kubeconfig);
        } else {
            res.status(404).send()
        }
    } catch (err) {
        console.log(err);
        res.status(500).render("error.hbs", {message: err});
    }
}

export async function get_cluster(req: express.Request<any>, res: express.Response<any>) {
    try {
        const cdef_name = req.params.cluster;
        const cdef = get_clusterdef(cdef_name, req.user?.username);
        if (! cdef) {
            res.status(404).render("error.hbs", {message: `Cluster ${cdef_name} not found.`});
            return;
        }
        const annotations = cdef?.metadata?.annotations
        let errors: string[][] = [];
        if (annotations && annotations['kanod.io/errors']) {
            const error_string = annotations['kanod.io/errors'];
            errors = JSON.parse(error_string);
        }
        const namespace = cdef?.spec?.multiTenant ? `${Namespace}-${cdef_name}` : Namespace
        const state = cdef?.status?.phase;
        console.log('Found parameter ' + cdef_name);

        const nodes = await clusterNodes(cdef_name, namespace);
        const replicas = await get_replicas(cdef_name);
        res.render(
            "cluster.hbs",
            {
                title: `Cluster ${cdef_name}`,
                user: req.user?.username,
                isAdmin: req.user?.username == 'admin',
                bmpSupport: use_baremetalpools,
                refresh: true,
                scripts: [],
                clusterdef: cdef_name,
                cluster: cdef_name,
                state: state,
                nodes: nodes,
                replicas: replicas,
            }
        );
    } catch (err) {
        console.log(err)
        res.status(500).render("error.hbs", {message: err});
    }
}
