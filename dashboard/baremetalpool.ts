/*
Copyright 2022 Orange.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

import * as express from "express";
import * as k8s from "@kubernetes/client-node";
import {kc, customApi} from "./core";
import { cdefs, Namespace } from "./clusterdefs";
import { use } from "passport";

const BMP_GROUP = 'bmp.kanod.io'
const BMP_VERSION = 'v1'
const BMP_PLURAL = 'baremetalpools'

export let use_baremetalpools = false;

// Will fail if CR does not exist.
customApi.listClusterCustomObject(BMP_GROUP, BMP_VERSION, BMP_PLURAL).then(
    (_) => {
        console.log('- baremetalpools supported.');
        use_baremetalpools = true;
    }
).catch((_) => { console.log('- no support for baremetalpools.'); })

export interface BaremetalPool {
    'apiVersion'?: string;
    'kind'?: string;
    'metadata'?: k8s.V1ObjectMeta;
    'spec'?: BaremetalPoolSpec;
    'status'?: BaremetalPoolStatus;
}

export interface BaremetalPoolSpec {
    poolname: string;
    size: number;
    maxServers: number;
    address: string;
    redfishschema: string;
}

export interface BaremetalPoolStatus {
    bmhcount: number;
    bmhnameslist: string[];
    status: string;
}

const listBaremetalpools = () => <any> customApi.listClusterCustomObject(
    BMP_GROUP, BMP_VERSION, BMP_PLURAL);

const baremetalpools: { [key: string]: BaremetalPool } = {};

function add_bmpool(obj: any) {
    const key = obj?.metadata?.namespace + '/' + obj?.metadata?.name;
    if (key) baremetalpools[key] = obj;
}

function del_bmpool(obj:any) {
    const key = obj?.metadata?.namespace + '/' + obj?.metadata?.name;
    if (key) {
        delete baremetalpools[key];
    }
}

export function watch_baremetalpools() {
    if (!use_baremetalpools) {
        return;
    }
    const bmpoolInformer = k8s.makeInformer<any>(
        kc, `/apis/${BMP_GROUP}/${BMP_VERSION}/${BMP_PLURAL}`,
        listBaremetalpools);
    bmpoolInformer.on('add', add_bmpool);
    bmpoolInformer.on('update', add_bmpool);
    bmpoolInformer.on('delete', del_bmpool);
    bmpoolInformer.on('error', (err: k8s.V1Pod) => {
        console.error(err);
        setTimeout(() => { bmpoolInformer.start(); }, 5000);
    });
    bmpoolInformer.start();
}

export function get_baremetalpools(req: express.Request<any>, res: express.Response<any>) {
    try {
        const pools: BaremetalPool[] = [];
        const user = req.user?.username;
        for(const pool_name in baremetalpools) {
            const pool = baremetalpools[pool_name]
            const ns = pool.metadata?.namespace
            if (!ns) continue;
            // Use the invariant that clusters are in a namespace prefix-clustername
            // where prefix is the name of the namespace for clusterdefs.
            // extract the coresponding clusterdef and check the users.
            const cdef = (ns.startsWith(Namespace + '-')) ?
                    cdefs[ns.slice(Namespace.length + 1)] : null;
            if (user && (user == 'admin' || (cdef && cdef.users.indexOf(user) >= 0))) {
                pools.push(pool);
            }
        }
        res.render(
            "baremetalpools.hbs",
            {
                title: "Pools",
                user: req.user?.username,
                isAdmin: req.user?.username == 'admin',
                refresh: true,
                scripts: [],
                pools: pools
            }
        );
    } catch (err) {
        res.render("error.hbs", {message: err})
    }
}

export function get_baremetalpool(req: express.Request<any>, res: express.Response<any>) {
    const user = req.user?.username;
    const pool_name = req.params.pool;
    const namespace = req.params.namespace;
    const cdef = (namespace.startsWith(Namespace + '-')) ? cdefs[namespace.slice(Namespace.length + 1)] : null;
    if (!(user && (user == 'admin' || (cdef && cdef.users.indexOf(user) >= 0)))) {
        res.status(404).render("error.hbs", {message: `BaremetalPool ${pool_name} not found.`});
        return;
    }
    const key = namespace + '/' + pool_name;
    const pool = baremetalpools[key];
    if (!pool) {
        res.status(500).render("error.hbs", {message: `Cannot find baremetalpool ${pool_name}`});
    }  else {
        res.render(
            "baremetalpool.hbs",
            {
                title: `Bare Metal Host ${pool_name}` ,
                user: req.user?.username,
                isAdmin: req.user?.username == 'admin',
                refresh: true,
                scripts: [],
                pool: pool,
            });
    }
}