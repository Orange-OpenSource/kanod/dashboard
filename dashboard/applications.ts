/*
Copyright 2021 Orange.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

import * as express from "express";
import { use_baremetalpools } from "./baremetalpool";
import {customApi} from "./core";

/** Show  the available apps. */
export async function get_apps(req: express.Request<any>, res: express.Response<any>) {
    try {
        const apps = await customApi.listNamespacedCustomObject(
            "argoproj.io", "v1alpha1", "argocd", "applications");
        res.render(
            "apps.hbs",
            {
                title: `Argo Applications`,
                user: req.user?.username,
                isAdmin: req.user?.username == 'admin',
                bmpSupport: use_baremetalpools,
                scripts: [],
                apps: apps.body,
            }
        );
    } catch (err) {
        res.status(500).send("<h1>Error</h1>" + String(err));
    }
}

