/*
Copyright 2021 Orange.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

import * as express from "express";
import * as k8s from "@kubernetes/client-node";
import {kc, customApi, coreApi} from "./core";
import { V1Secret } from "@kubernetes/client-node";
import { use_baremetalpools } from "./baremetalpool";

export interface ClusterDef {
    'apiVersion'?: string;
    'kind'?: string;
    'metadata'?: k8s.V1ObjectMeta;
    'spec'?: ClusterDefSpec;
    'status'?: ClusterDefStatus;
}

export interface GitLocation {
    repository: string;
    branch?: string;
    path?: string;
}

export interface PivotInfo {
    kanodName: string;
    kanodNamespace: string;
    pivot: boolean;
}

export interface ClusterDefSpec {
    source: GitLocation;
    credentialName: string;
    configurationName: string;
    cidataName: string;
    multiTenant: boolean;
    pivotInfo: PivotInfo;
}

export interface ClusterDefStatus {
    controlPlaneVersion: string;
    phase: string;
}

export interface ClusterDefCondition {
    type: string;
    status: string;
    lastProbeTime: Date;
}

interface CDef {
    clusterdef: ClusterDef;
    users: string [];
}

export const Namespace = process.env.CLUSTERDEF_NAMESPACE || 'capm3';

export const cdefs: { [key: string]: CDef } = {};
const secrets: {[key: string]: CDef} = {};

async function add_clusterdef(obj: ClusterDef) {
    const cdefName = obj?.metadata?.name;
    console.log(`ADD ${cdefName}`);
    if (!cdefName) return;
    const credName = obj?.spec?.credentialName;
    console.log(`SECRET: ${credName}`);
    if (!credName) return;
    try {
        const creds = await coreApi.readNamespacedSecret(credName, Namespace);
        const users_base64 = creds.body?.data?.users;
        const users = (users_base64) ? Buffer.from(users_base64, 'base64').toString('ascii') : undefined;
        console.log(`USERS: ${users}`);
        const splitted = users?.split(',') || [];
        const cell = {
            clusterdef: obj,
            users: splitted,
        }
        cdefs[cdefName] = cell;
        secrets[credName] = cell;
    } catch (e) {
        console.log(e);
    }
}

function del_clusterdef(obj: ClusterDef) {
    const cdefName = obj?.metadata?.name;
    const credName = obj?.spec?.credentialName;
    console.log(`DELETE ${cdefName}`);
    if (cdefName) {
        delete cdefs[cdefName];
    }
    if (credName) {
        delete secrets[credName];
    }
}

function add_secret(obj: V1Secret) {
    const cdefName = obj?.metadata?.name;
    if (!cdefName) {
        return;
    }
    const cell = secrets[cdefName];
    if (!cell) {
        return;
    }
    const users = obj?.data?.users;
    console.log(`USERS: ${users}`);
    cell.users = users?.split(',') || [];
}

function del_secret(obj: V1Secret) {
    const cdefName = obj?.metadata?.name;
    if (!cdefName) {
        return;
    }
    const cell = secrets[cdefName];
    if (cell) {
        cell.users = [];
    }
}

const listCdefs = () => <any> customApi.listNamespacedCustomObject(
    "gitops.kanod.io", "v1alpha1", Namespace, "clusterdefs");

const cdefInformer = k8s.makeInformer<ClusterDef>(
    kc, `/apis/gitops.kanod.io/v1alpha1/namespaces/${Namespace}/clusterdefs`, listCdefs);

cdefInformer.on('add', add_clusterdef);

cdefInformer.on('update', (obj: any) => {
    del_clusterdef(obj);
    add_clusterdef(obj);
});

cdefInformer.on('delete', del_clusterdef);

cdefInformer.on('error', (err: ClusterDef) => {
    console.error(err);
    setTimeout(() => { cdefInformer.start(); }, 5000);
});

cdefInformer.start();

const listSecrets = () => coreApi.listNamespacedSecret(Namespace);

const secretInformer = k8s.makeInformer<V1Secret>(
    kc, `/api/v1/namespaces/${Namespace}/secrets`, listSecrets);

secretInformer.on('add', add_secret);

secretInformer.on('update', add_secret);

secretInformer.on('delete', del_secret);

secretInformer.on('error', (err: ClusterDef) => {
    console.error(err);
    setTimeout(() => { secretInformer.start(); }, 5000);
});

secretInformer.start();

export function get_clusterdef(
    cluster : string,
    user: string | undefined
): ClusterDef | undefined {
    const cell = cdefs[cluster];
    if (cell && user && (user == 'admin' || cell.users.indexOf(user) >= 0)) {
        return cell.clusterdef;
    }
    return;
}

export async function get_clusters(
    req: express.Request<any>,
    res: express.Response<any>,
) {
    const clusterdefs: ClusterDef[] = [];
    const user = req.user?.username;
    for (const key in cdefs) {
        const cell = cdefs[key];
        if (cell && user && (user == 'admin' || cell.users.indexOf(user) >= 0)) {
            clusterdefs.push(cell.clusterdef);
        }
    }
    try {
        res.render(
            "clusters.hbs",
            {
                title: `Cluster Definitions`,
                user: req.user?.username,
                isAdmin: req.user?.username == 'admin',
                bmpSupport: use_baremetalpools,
                scripts: [],
                clusterdefs: clusterdefs,
            }
        );
    } catch (err) {
        res.status(500).render("error.hbs", {message: err});
    }
}
