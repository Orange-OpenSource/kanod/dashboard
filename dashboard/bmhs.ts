/*
Copyright 2021 Orange.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

import * as express from "express";
import * as k8s from "@kubernetes/client-node";
import {kc, customApi} from "./core";
import { cdefs, Namespace } from "./clusterdefs";
import { use_baremetalpools } from "./baremetalpool";

const listBmhs = () => <any> customApi.listClusterCustomObject(
    "metal3.io", "v1alpha1", "baremetalhosts");

const bmhInformer = k8s.makeInformer<any>(
    kc, '/apis/metal3.io/v1alpha1/baremetalhosts', listBmhs);

const bmhs: { [key: string]: any } = {};

function add_bmh(obj: any) {
    const key = obj?.metadata?.namespace + '/' + obj?.metadata?.name;
    if (key) bmhs[key] = obj;
}

function del_bmh(obj:any) {
    const key = obj?.metadata?.namespace + '/' + obj?.metadata?.name;
    if (key) {
        delete bmhs[key];
    }
}

bmhInformer.on('add', add_bmh);

bmhInformer.on('update', add_bmh);

bmhInformer.on('delete', del_bmh);

bmhInformer.on('error', (err: k8s.V1Pod) => {
    console.error(err);
    setTimeout(() => { bmhInformer.start(); }, 5000);
});

bmhInformer.start();

export function get_bmhs(req: express.Request<any>, res: express.Response<any>) {
    try {
        res.render(
            "bmhs.hbs",
            {
                title: "Bare Metal Hosts",
                user: req.user?.username,
                isAdmin: req.user?.username == 'admin',
                refresh: true,
                scripts: [],
                bmhs: Object.values(bmhs)
            }
        );
    } catch (err) {
        res.render("error.hbs", {message: err})
    }
}

export function get_bmh(req: express.Request<any>, res: express.Response<any>) {
    const bmh_name = req.params.bmh;
    const namespace = req.params.namespace;
    const user = req.user?.username;
    const cdef = (namespace.startsWith(Namespace + '-')) ? cdefs[namespace.slice(Namespace.length + 1)] : null;
    if (!(user && (user == 'admin' || (cdef && cdef.users.indexOf(user) >= 0)))) {
        res.status(404).render("error.hbs", {message: `Cannot find baremetal host ${bmh_name}`});
        return;
    }
    const key = namespace + '/' + bmh_name;
    const bmh = bmhs[key];
    if (!bmh) {
        res.status(404).render("error.hbs", {message: `Cannot find baremetal host ${bmh_name}`});
        return
    }
    res.render(
        "bmh.hbs",
        {
            title: `Bare Metal Host ${bmh_name}` ,
            user: req.user?.username,
            isAdmin: req.user?.username == 'admin',
            bmpSupport: use_baremetalpools,
            refresh: true,
            scripts: [],
            bmh: bmh
        });
}
