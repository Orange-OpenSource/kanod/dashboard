Installation
============

The Dashboard is a pod running inside the Kanod stack. The docker image is built
and uploaded to the Nexus using ``build.sh``.

The Dashboard accesses many privileged information in the stack and requires
a security policy defined in ``manifest.yml``.
