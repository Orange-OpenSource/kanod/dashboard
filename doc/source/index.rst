.. Kanod Dashboard documentation master file, created by
   sphinx-quickstart on Thu Sep  2 14:20:21 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Kanod Dashboard
===============

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   usage

