Usage
=====

The service is exported by the life-cycle manager to be visible on its
standard https port.

The dashboard presents a synthetic view of:

* the baremetal hosts managed by the Kanod instance
* the clusters created by the Kanod instance

Authentication
--------------

Authentication on the dashboard is based on password hashes stored in a
secret ``dashboard-auth``in the ``argocd`` namespace.

The entry ``passwords`` must be a file where each line contains a login and
a bcrypt password hash separated by a colon. The file **must** contain an
entry for the admin user.

Authorisation for the clusters is based on the content of the field ``users``
in the secret referenced by the field ``credentialName`` in the ``clusterdef``
CRD associated to the cluster. This field must be a comma separated list of
users login.

Admin view
----------
The admin user can view the state of bare-metal hosts from the dashboard.

He can also access the argocd user interface. There is a separate login but
the admin password is the same as the admin password for the dashboard *after
the admin user logged on the dashboard for the first time*.

The admin user has also a view on all the clusters defined.

Cluster administrators view
----------------------------
Cluster administrator can access the clusters for which they are registered
as users. From the cluster view, the administrator can access the
administrator **kubeconfig** of the cluster.
